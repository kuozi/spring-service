package com.tools;

import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class Tools_Mongo {
    private static final Logger logger=Logger.getLogger(Tools_Mongo.class.getName());

    public static String login(String name, String pwd) {
        String ret = "fail";
        MongoClient mc = null;
        DB db = null;
        DBCollection coll = null;

        try {
            mc = new MongoClient("localhost", 27017);
            db = mc.getDB("android");
            coll = db.getCollection("android_user");
            DBObject query = new BasicDBObject();

            query.put("name", new BasicDBObject("$eq", name));
            query.put("pwd", new BasicDBObject("$eq", pwd));
            DBCursor res = coll.find(query);

            if (res.hasNext()) {
                ret = "success";
                logger.info(res.next().toString());
            }
        } catch (UnknownHostException e) {
            logger.error("fial", e);
        }
        return ret;

    }

    public static String register(String name, String pwd, String info) {
        String ret = "fail";
        String find = findOne(name);
        if ("fail".equals(find)) {
            MongoClient mc = null;
            DB db = null;
            DBCollection coll = null;

            try {
                mc = new MongoClient("localhost", 27017);
                db = mc.getDB("android");
                coll = db.getCollection("android_user");
                DBObject dbo = new BasicDBObject();
                dbo.put("name", name);
                dbo.put("pwd", pwd);
                dbo.put("info", info);
                coll.save(dbo);
                ret = "success";
            } catch (Exception e) {
                logger.error("fial", e);
            }
        } else {
            ret = "exits";
        }
        return ret;
    }

    public static String findOne(String name) {
        String ret = "fail";
        MongoClient mc = null;
        DB db = null;
        DBCollection coll = null;

        try {
            mc = new MongoClient("localhost", 27017);
            db = mc.getDB("android");
            coll = db.getCollection("android_user");

            DBObject query = new BasicDBObject();
            query.put("name", new BasicDBObject("$eq", name));

            DBCursor res = coll.find(query);
            if (res.hasNext()) {
                ret = res.next().toString();
            }
        } catch (Exception e) {
            logger.error("fial", e);
        }
        return ret;
    }

    public static JSONArray findAll() {
        MongoClient mc = null;
        DB db = null;
        DBCollection coll = null;
        JSONArray jsonArray = new JSONArray();

        try {
            mc = new MongoClient("localhost", 27017);
            db = mc.getDB("android");
            coll = db.getCollection("android_user");

            DBCursor res = coll.find();
            while (res.hasNext()) {
                String ret = res.next().toString();
                jsonArray.put(new JSONObject(ret));
            }
        } catch (Exception e) {
            logger.error("fial", e);
        }
        return jsonArray;
    }
}
