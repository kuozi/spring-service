package com.android.rest.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

public class Listener implements ServletContextListener{
    private static final Logger logger=Logger.getLogger(Listener.class.getName());
    public void contextInitialized(ServletContextEvent sce) {
        logger.info("程序启动####################################");
    }

    public void contextDestroyed(ServletContextEvent sce) {
        //在程序挂掉时执行 使用IDE看不出
        logger.info("程序停止####################################");  
    }
}
