package com.android.rest.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pojo.User;
import com.tools.Tools_Mongo;

@Controller
public class FindController {
    Logger logger = Logger.getLogger(FindController.class.getName());

    @RequestMapping(value = "/findjsp/{name}", method = RequestMethod.GET)
    public String finOneByName(ModelMap map, @PathVariable String name) throws UnsupportedEncodingException {
        logger.info("----------------------" + name + "----------------------");
        try {
            String strUser = Tools_Mongo.findOne(name);
            JSONObject jsonUser = new JSONObject(strUser);
            User user = new User();
            user.setName(jsonUser.get("name").toString());
            user.setPwd(jsonUser.get("pwd").toString());
            user.setInfo(jsonUser.get("info").toString());
            map.addAttribute("user", user);
        } catch (Exception e) {
            logger.error("查询失败",e);
        }
        return "userDetail";
    }

    @ResponseBody
    @RequestMapping(value = "/find", method = RequestMethod.POST)
    public String finOneByName(String name) throws UnsupportedEncodingException {
        logger.info("----------------------" + name + "----------------------");
        String strUser = Tools_Mongo.findOne(name);
        strUser = new String(strUser.getBytes("UTF-8"), "ISO-8859-1");
        return strUser;
    }

    @RequestMapping(value = "/findjsp/all", method = RequestMethod.GET)
    public String finALL(ModelMap map) {
        logger.info("----------------------所有用户----------------------");
        try {
            JSONArray jsonUsers = Tools_Mongo.findAll();
            List<User> users = new ArrayList<>();

            for (int i = 0; i < jsonUsers.length(); i++) {
                User user = new User();
                user.setName(jsonUsers.getJSONObject(i).get("name").toString());
                user.setPwd(jsonUsers.getJSONObject(i).get("pwd").toString());
                user.setInfo(jsonUsers.getJSONObject(i).get("info").toString());
                users.add(user);
            }
            map.addAttribute("users", users);
        } catch (Exception e) {
            logger.error("查询失败", e);
        }
        return "allUser";
    }

    @ResponseBody
    @RequestMapping(value = "/find/all", method = RequestMethod.POST)
    public String finAll() throws UnsupportedEncodingException {
        logger.info("----------------------所有用户----------------------");
        JSONArray strUser = Tools_Mongo.findAll();
        String resUsers = strUser.toString();
        resUsers = new String(resUsers.getBytes("UTF-8"), "ISO-8859-1");
        return resUsers;
    }
}
