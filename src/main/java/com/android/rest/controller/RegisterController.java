package com.android.rest.controller;

import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tools.Tools_Mongo;

@Controller
public class RegisterController {
    private static final Logger logger=Logger.getLogger(RegisterController.class.getName());
    @ResponseBody
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(String name,String pwd,String info) throws UnsupportedEncodingException {
        logger.info("----------------------" + name + "--注册--" + pwd + "----------------------");
        return Tools_Mongo.register(name, pwd, info);
    }
}
