package com.android.rest.controller;

import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tools.Tools_Mongo;

@Controller
public class LoginController {
    private static final Logger logger=Logger.getLogger(LoginController.class.getName());
    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    private String login(ModelMap map, String name, String pwd) throws UnsupportedEncodingException {
        logger.info("----------------------" + name + "--登录--" + pwd + "----------------------");
        //System.out.println("----------------------" + name + "--登录--" + pwd + "----------------------");
        return Tools_Mongo.login(name, pwd);
    }
}
