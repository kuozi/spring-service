<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="<c:url value='/css/bootstrap.css'/>" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>android测试</title>

</head>
<body>
	<div class="container">
		<lable>
		<h4>你好:${user.name}!</h4>
		</lable>
		<table class="table">
			<tr>
				<td>姓名：</td>
				<td>${user.name}</td>
			</tr>
			<tr>
				<td>密码：</td>
				<td>${user.pwd}</td>
			</tr>
			<tr>
				<td>说明：</td>
				<td>${user.info}</td>
			</tr>
		</table>
	</div>
</html>