<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="<c:url value='/css/bootstrap.css'/>" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>所有用户</title>
</head>
<body class="container">
	<table class="table">
		<tr>
			<th>姓名</th>
			<th>密码</th>
			<th>说明</th>
		</tr>
		<c:forEach items="${users}" var="user">
			<tr>
				<td>${user.name}</td>
				<td>${user.pwd}</td>
				<td>${user.info}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>